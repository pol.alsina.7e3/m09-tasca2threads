﻿using System;
using System.Threading;

namespace Tasca2Thread
{
    class Program
    {
        static void Main(string[] args)
        {
            //Exercici1();
            Exercici2();
        }
        static void Exercici1()
        {
            Thread thread1 = new Thread(() => PrintMessage("Hi havia una vegada"));
            Thread thread2 = new Thread(() => { thread1.Join(); PrintMessage("En un lugar de la Mancha"); });
            Thread thread3 = new Thread(() => { thread2.Join(); PrintMessage("Once upon a time"); });
            thread1.Start();
            thread2.Start();
            thread3.Start();
        }
        static void PrintMessage(string message) {
            Console.WriteLine(message);
        }
        static void Exercici2()
        {
            Nevera nevera = new Nevera();
            Thread Anitta = new Thread(() => nevera.OmplirNevera("Anitta"));
            Thread BadBunny = new Thread(() => { Anitta.Join(); nevera.BeureCervesa("Bad Bunny"); });
            Thread LilNasX = new Thread(() => { BadBunny.Join(); nevera.BeureCervesa("Lil Nas X"); });
            Thread ManuelTurizo = new Thread(() => { LilNasX.Join(); nevera.BeureCervesa("Manuel Turizo"); });
            Anitta.Start();
            BadBunny.Start();
            LilNasX.Start();
            ManuelTurizo.Start();
        }
    }
    public class Nevera
    {
        int cerveses;
        public Nevera() { cerveses = 6; }
        public void OmplirNevera(string nom)
        {
            Random random = new Random();            
            int cerAOmplir = random.Next(0,7);
            if ((cerAOmplir + cerveses) <= 9)
            {
                cerveses += cerAOmplir;
            }
            else
            {
                cerveses = 9;
            }
            Console.WriteLine(nom + " Ha omplert la nevera");
            Console.WriteLine("A la nevera hi han {0} cerveses",cerveses);
        }
        public void BeureCervesa(string nom)
        {
            Random rand = new Random();
            int cerARestar = rand.Next(0,7);
            if ((cerveses - cerARestar) > 0)
            {
                cerveses -= cerARestar;
            }
            else
            {
                cerveses = 0;
            }
            Console.WriteLine(nom + " Ha begut {0} cerveses", cerARestar);
            Console.WriteLine("A la nevera hi han {0} cerveses", cerveses);
        }
    }
}
